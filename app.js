const connect = require('connect');
const serveStatic = require('serve-static');
connect().use(serveStatic(__dirname)).listen(4321, function(){
    console.log('Server running on 4321...')
})


/*
const http = require('http')
const fs = require('fs')

const server = http.createServer((req, res) => {
    res.writeHead(200, {'Content-Type': 'text/html'})
    let stream = fs.createReadStream(__dirname+'/index.html','UTF-8')
    stream.pipe(res)
})

server.listen(3000)*/
