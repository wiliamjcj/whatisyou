let x = 20, y = 20, w = 30, h = 30
let x2 = 100, y2 = 100, w2 = 40, h2 = 40

let up, down, left, right, leftColision, rightColision, upColision, downColision
let stepSpeed = 2

function setup() {
    createCanvas(800, 600)
    //rectMode(CENTER)
}

function draw() {
    background(230)
    let colision = squareCollision(x,y,h,x2,y2,h2)
    
    rect(x,y,w,h)
    rect(x2,y2,w2,h2)

    if(!colision){
        leftColision = rightColision = upColision = downColision = false
    }

    if(left){
        x -= stepSpeed
        if(colision && !rightColision && !upColision  && !downColision && !squareFacingTopBottom(x,y,h,x2,y2,h2)){ 
            leftColision = true
            x2 -= stepSpeed
        }
    }else if(right){
        x += stepSpeed
        if(colision && !leftColision && !upColision  && !downColision && !squareFacingTopBottom(x,y,h,x2,y2,h2)){
            rightColision = true
            x2 += stepSpeed
        }
    }else if(up){
        y -= stepSpeed
        if(colision && !rightColision && !leftColision  && !downColision && !squareFacingLeftRight(x,y,h,x2,y2,h2)){ 
            upColision = true
            y2 -= stepSpeed
        }
    }else if(down){
        y += stepSpeed
        if(colision && !rightColision && !upColision  && !leftColision && !squareFacingLeftRight(x,y,h,x2,y2,h2)){
            downColision = true
             y2 += stepSpeed
        }
    } 
}

function squareFacingTopBottom(x,y,L,x2,y2,L2){
    return (y + L == y2 || y == y2 + L2)
}

function squareFacingLeftRight(x,y,L,x2,y2,L2){
    return (x == x2 + L2 || x + L == x2)
}

function circleCollision(x,y,d,x2,y2,d2){
    return int(dist(x,y,x2,y2)) < d/2 + d2/2 
}

function squareCollision(x,y,l,x2,y2,l2,includeCorners){
    return x2 <= x+l && y2 <= y+l && x2+l2 >= x && y2+l2 >= y
}

function sameCoord(x,y,x2,y2){
    return int(dist(x,y,x2,y2)) == 0
}

function keyPressed(){
    if(keyCode === DOWN_ARROW){
        down = true
    }else if(keyCode === UP_ARROW){
        up = true
    }else if(keyCode === LEFT_ARROW){
        left = true
    }else if(keyCode === RIGHT_ARROW){
        right = true
    }
}

function keyReleased(){
    if(keyCode === DOWN_ARROW){
        down = false
    }else if(keyCode === UP_ARROW){
        up = false
    }else if(keyCode === LEFT_ARROW){
        left = false
    }else if(keyCode === RIGHT_ARROW){
        right = false
    }
}
